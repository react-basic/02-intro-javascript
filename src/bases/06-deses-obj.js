// Desestructruacion
// Asignacion desestructurante

const persona = {
    nombre: 'Tony',
    edad: 45,
    clave: 'Ironman',
    rango: 'Soldado'
}

// const {edad, nombre, clave} = persona;

// console.log(nombre);
// console.log(edad);
// console.log(clave);

const usarContext = ({edad, nombre, clave, rango = 'Capitan'}) => {
    // console.log(edad, clave, nombre, rango);
    return {
        nombreClave: clave,
        anios: edad,
        latlng: {
            lat: 14.1213,
            lng: -12.5646654
        }
    }
}

const {nombreClave, anios, latlng:{lat, lng}} = usarContext(persona);

console.log(nombreClave, anios);
console.log(lat, lng);
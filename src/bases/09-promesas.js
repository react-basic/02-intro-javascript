import { getHeroeById } from "./bases/08-imp-ext";


// const promesa = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         const heroe = getHeroeById(2);
//         resolve(heroe)
//         // reject('No se pudo encotrar el héroe');
//     }, 2000);
// });

// promesa.then((heroe) => {
//     console.log('heroe', heroe);
// })
// .catch(err => console.warn(err));


const getHeroeByIdAsync = (id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const heroe = getHeroeById(id);

            if (heroe === undefined) {
                reject('El heroe no existe');
            } else {
                resolve(heroe)
            } 
            // reject('No se pudo encotrar el héroe');
        }, 2000);
    });
}

getHeroeByIdAsync(1)
    .then(console.log)
    .catch(console.warn);